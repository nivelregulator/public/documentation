# Nivel Regulator APIs

Here you will find all available documentation regarding the Nivel Regulator APIs

## Notes

* Hostnames and API keys will be given separate from this document

## Terms

* *MDS* - [Mobility Data Specification](https://github.com/openmobilityfoundation/mobility-data-specification)
* *OMF* - [Open Mobility Foundation](https://www.openmobilityfoundation.org/)
* *agency* - the city or any other body with regulatory authority
* *provider* - the company that operates the vehicles

# Versions

The MDS APIs are continuously being worked on. They're divided into different versions which can be considered stable.

Most of the implementations are very similar with few differences.

As of now, we will only support **v1.0.0** documented here:

* [Agency](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/agency)
* [Policy](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy)

### MDS Modifications

MDS specifies that APIs must handle requests for specific versions of the specification from clients.

**We do not support this** and we don't handle the header described in the MDS specs
```
Accept: application/vnd.mds.provider+json;version=x.y
```

Instead we prefix the url endpoint with the version we are using.


# Authentication

All API endpoints are closed to the public, so you need to authenticate before consuming the API.

### MDS Modifications

MDS specifies that JWT tokens should be used for authenticating.

**We do not support this** and instead implement this by using the HTTP header `X-API-Key` with an API key (uuid) on each request.

Each provider will get unique API keys. These work as passwords to access the API, hence they must be stored safely.

### Testing

Before making any real requests, you can test the following endpoints to check if your connection to the API server works.

* public
    * `GET https://<host>/ping`
        * 200 - "pong"

* private
    * `GET https://<host>/authorized`
        * 401 - "Unauthorized."
        * 200 - "Authorized!"


## Nivel's APIs

Nivel's APIs implement a slightly ***modified*** version of OMF's MDS API. The following documents describes this in detail

* [Agency API](./agency-api.md)
* [Policy API](./policy-api.md)


