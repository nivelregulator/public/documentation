# Nivel Regulator Policy API

## MDS Modifications

All returned **json data** are mostly MDS compatible. There is one exception regarding `rate_recurrence`.

There are also some other changes not related to the data objects/properties.

This is all documented here.

### Mutable policies

MDS considers policies to be legal documents and therefore defines them as **immutable** (a policy object **cannot** be changed). For now, we will simplify this and make our policies dynamically **mutable** (a policy object **can** be changed).

The reason behind this is that minor, frequent policy updates (like a small change in the polygons of a zone) are more complex to implement (for providers and for the regulator) when they are immutable.

It makes little sense to create new policies every time there is a slight change. During the pilot phase 1 we will probably only have 3-4 different policies (described below) which shouldn't change much.

Each API response will contain a top level property `updated` which will keep track of whether any of the policies have been changed.

In effect the API will always return the current state of all policies.

We recommend polling the API a couple of times each day.

### Query parameters

MDS supports two query parameters that we won't handle

* `start_date`
* `end_date`

This is a direct consequence of making the policies mutable, as we will only show the current active policies.

### Policy object properties

Another direct consequence of making the policies mutable is that the following properties don't really have any meaning

* `start_date`
* `end_date`
* `published_date`
* `prev_policies`

`start_date` and `published_date` will however be provided since they are required by the MDS specs.

### Rate Recurrences

We will publish rates as `<value>` NOK (øre) per `24h`. However, we will only charge per complete minute.

MDS doesn't support this, so we solve it by adding a custom value fore `rate_recurrence` namely **per_complete_minute**.

Look at the description of the rate policy below for details and an example.

# Policies

For now, there are only 3 different policies we want to communicate to the providers.

## 1. Parking forbidden

Zones where vehicles are either allowed to park or not. This is implemented with the following rules:

* Parking allowed
  * `rule_type`: "count"
  * `maximum`: an integer greater than 0
  * `states`: [ "reserved", "available", "non_operational" ]
* Parking not allowed
  * `rule_type`: "count"
  * `maximum`: 0
  * `states`: [ "reserved", "available", "non_operational" ]

## 2. Prohibited area

We will provide zones where vehicles are prohibited from parking and traveling. This is implemented with the following rule:

* `rule_type`: "count"
* `maximum`: 0
* `states`: [ "reserved", "available", "non_operational", "on_trip" ]

## 3. Parking rate / fee

We will provide zones where a fee or subsidy is applied for parked vehicles. This is implemented with the following rule:

* `rule_type`: "rate"
* `rule_units`: "days"
* `rate_amount`: an integer other than 0, positive for fee, negative for subsidy (amount in NOK øre)
* `rate_recurrence`: "per_complete_minute"
* `states`: [ "reserved", "available", "non_operational" ]

### Example

A vehicle will be charged/subsidised according to the amount of minutes it has been parked in a zone with a set rate. E.g. 300 minutes a specific zone with a daily rate of 5 NOK. The fee/subsidy (aka rate) is then calculated like this:

> `300 * 5 / (24 * 60) = 1.04 NOK` or `104 NOK (øre)`

## 4. Speed limit

We will provide zones where vehicles must obey a speed limit. This is implemented with the following rule:

* `rule_type`: "speed"
* `rule_units`: "kph"
* `maximum`: an integer greater than 0
* `states`: [ "on_trip" ]

## 5. Max Vehicles

We will provide zones with Maximum number of vehicles allowed for a provider. This is implemented with the following rule:

* `rule_type`: "count"
* `rule_units`: "devices"
* `maximum`: integer
* `vehicle_types`: [ "scooter" ]
* `states`: [ "available", "non_operational", "reserved", "on_trip"]

## Time variations

The policies **Parking rate** and **Speed limit** above can have extra rules with values bound by a time interval. This is implemented by adding the following properties:

* `days`: [ "mon" ]
* `start_time`: "HH:MM:SS"
* `end_time`: "HH:MM:SS"

# Overlapping zones

Zones may overlap each other. MDS has a concept of [Order of Operations](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#order-of-operations) which describes how some zones (or more precisely rules) precede others.

The basics of this is that a rule (with zones) with a lower array index would take precedence over a rule with a higher index.

### Example

The above logic applies to parking restrictions:

>A large zone where vehicles **are not** allowed to park. However, within this zone we have many smaller parking zones where vehicles **are** allowed to park.

# Response

All responses have these main properties. The `data` field will contain relevant data related to the endpoint.

```json
{
  "version": "1.0.0",
  "updated": 1605102179000,
  "data": {
    // Endpoint data
  }
}
```

# Policies endpoints

Please read the following paragraphs from the MDS documentation to get a basic understanding

* [Policies](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#policies)
* [Policy object](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#policy)
* [Policy rules](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#rules)


### Endpoints

* `GET https://<host>/mds/<version>/policies`
  * In the response, `data` will contain a property `policies` with an array of policy objects
  * [Example json](./examples/policies.json)
* `GET https://<host>/mds/<version>/policies/{id}`
  * In the response, `data` will contain a property `policy` with a policy object
  * [Example json](./examples/policy.json)


# Geographies endpoints

Please read the following paragraphs from the MDS documentation to get a basic understanding

* [Geographies](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#geographies)
* [Geography object](https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-1.0.0/policy#geography)


### Endpoints

* `GET https://<host>/mds/<version>/geographies`
  * In the response, `data` will contain a property `geographies` with an array of GeoJSON objects
  * [Example json](./examples/geographies.json)
* `GET https://<host>/mds/<version>/geographies/{id}`
  * In the response, `data` will contain a property `geography` with a GeoJSON object
  * [Example json](./examples/geography.json)

