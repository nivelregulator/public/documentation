# Nivel Regulator Agency API

**UPDATE!**

We are supporting all MDS v1.0.0 endpoints except `/stops` which is a beta feature

# Vehicles

`GET https://<host>/mds/<version>/vehicles`

This endpoint is supported, even though it only returns an empty object, for now.

`GET https://<host>/mds/<version>/vehicles/{device_id}`

This endpoint is partly supported, `state`, `prev_events`, `updated` is currently not returned. 

```json
{
  "device_id":"abcdefgh-1234-5678-9012-b30ff9e06167",
  "provider_id":"not available",
  "vehicle_id":"123",
  "vehicle_type":"scooter",
  "propulsion_types":["human","electric"],
  "year":null,
  "mfgr":null,
  "model":null,
  "state":"NOT IMPLEMENTED",
  "prev_events":"NOT IMPLEMENTED",
  "updated":"NOT IMPLEMENTED"}
```

# Vehicle - Register

`POST https://<host>/mds/<version>/vehicles`

This endpoint is supported.

# Vehicle - Update

`PUT https://<host>/mds/<version>/vehicles/{device_id}`

This endpoint is supported.

# Vehicle - Event

`POST https://<host>/mds/<version>/vehicles/{device_id}/event`

Please read the following documents to understand the basics of MDS event data

* [MDS Vehicle Event](https://github.com/openmobilityfoundation/mobility-data-specification/blob/release-1.0.0/agency/README.md#vehicle---event)
* [MDS Vehicle States](https://github.com/openmobilityfoundation/mobility-data-specification/blob/release-1.0.0/general-information.md#vehicle-states)
* [MDS Vehicle State Events](https://github.com/openmobilityfoundation/mobility-data-specification/blob/release-1.0.0/general-information.md#vehicle-state-events)

Events are to be pushed once they happen.

## Examples

### User starts and ends a trip:

```json
{
  "vehicle_state": "on_trip",
  "event_types": [
    "trip_start"
  ],
  "timestamp": 1616955079300,
  "telemetry": {
     // Remember each event must include a Telemetry object
 },
  "trip_id": "d2cd1013-dc7a-45d7-aa69-9ae0a81197dc"
}
```

```json
{
  "vehicle_state": "available",
  "event_types": [
    "trip_end"
  ],
  "timestamp": 1600754753245,
  "telemetry": {
    // Remember each event must include a Telemetry object
  },
  "trip_id": "d2cd1013-dc7a-45d7-aa69-9ae0a81197dc"
}}
```

### A vehicle has low battery

```json
{
  "vehicle_state": "non_operational",
  "event_types": [
    "battery_low"
  ],
  "timestamp": 1608225330570,
  "telemetry": {
    // Remember each event must include a Telemetry object
  },
  "trip_id": null
}
```
### A vehicle is removed from the street

```json
{
  "vehicle_state": "removed",
  "event_types": [
    "maintenance_pick_up"
  ],
  "timestamp": 1618441545929,
  "telemetry": {
     // Remember each event must include a Telemetry object
  },
  "trip_id": null
}
```

### The provider lost communication with a vehicle and then restored it

```json
{
  "vehicle_state": "unknown",
  "event_types": [
    "comms_lost"
  ],
  "timestamp": 1615025418716,
  "telemetry": {
     // Remember each event must include a Telemetry object
  },
  "trip_id": null
}
```

```json
{
  "vehicle_state": "on_trip",
  "event_types": [
    "comms_restored"
  ],
  "timestamp": 1592649954598,
  "telemetry": {
     // Remember each event must include a Telemetry object
  },
  "trip_id": null
}
```

## Parked

MDS does not define a `parked` state so this is something we (agency) define based on MDS states you (provider) send us.

We use this state in the follow manner:

* We will calculate street rent per minute in a zone based on how long a vehicle has been parked there.
* We can prohibit vehicles from parking in a particular zone and have other smaller zones on top dedicated for parking.

These states determine how we consider a vehicle parked or not:

* **Parked**
  * available
  * non_operational
  * reserved
* **Not parked**
  * removed
  * on_trip
  * unknown

# Vehicles - Telemetry

`POST https://<host>/mds/<version>/vehicles/telemetry`

Please read the following document to understand the basics of MDS telemetry data
* [MDS Vehicle Telemetry](https://github.com/openmobilityfoundation/mobility-data-specification/blob/release-1.0.0/agency/README.md#vehicle---telemetry)

## Notes

* Device id **must** be a unique UUID for each vehicle.
* Timestamps **must** be in **milliseconds** (**not** seconds) since Unix epoch.
* Data **must** be sent as one request with a batch of vehicles, **not** one request per vehicle.
* Push event data, including telemetry, with no latency. 
* Push telemetry at least once every 24 hours, if there are no event update on a vehicle.

## Examples

### Minimum required data set

```json
{
  "data": [
    {
      "device_id": "8bffe0e8-45e9-46f2-b73b-7a9c2b694eea",
      "timestamp": 1601644317782,
      "gps": {
        "lat": 58.9632,
        "lng": 5.7297
      },
      "charge": 0.72
    },
    {
      "device_id": "a7f67b9c-5b5d-433e-ba80-d324382fc124",
      "timestamp": 1601644317786,
      "gps": {
        "lat": 58.9534,
        "lng": 5.7067
      },
      "charge": 0.66
    },
    // ...
  ]
}
```

### All supported data properties

MDS also specifies other optional data details in the `gps` object. If you have data on any of these you should also send it.

```json
{
  "data": [
    {
      "device_id": "8bffe0e8-45e9-46f2-b73b-7a9c2b694eea",
      "timestamp": 1601644317782,
      "gps": {
        "lat": 58.9632,
        "lng": 5.7297,
        "altitude": 59.47,
        "heading": 284.7,
        "speed": 2.79,
        "accuracy": 2.67,
        "hdop": 90.13,
        "satellites": 3
      },
      "charge": 0.72
    },
    {
      "device_id": "a7f67b9c-5b5d-433e-ba80-d324382fc124",
      "timestamp": 1601644317786,
      "gps": {
        "lat": 58.9534,
        "lng": 5.7067,
        "altitude": 37.92,
        "heading": 219.13,
        "speed": 2.58,
        "accuracy": 4.59,
        "hdop": 36.83,
        "satellites": 2
      },
      "charge": 0.66
    },
    // ...
  ]
}
```

# Stops

`POST https://<host>/mds/<version>/stops`

This endpoint is **not supported at all**  (MDS beta feature).

