# Nivel MDS v2.0.0 APIs

Here you will find all available documentation regarding the Nivels implementation of MDS v2.0.0

# Getting started

This document assumes you already have a general understanding of how MDS works, so..

1. Please **read the official documentation first**.

    * https://www.openmobilityfoundation.org/about-mds/
    * https://github.com/openmobilityfoundation/mobility-data-specification/tree/release-2.0.0
    * https://openmobilityfnd.stoplight.io/


2. Ask us for access to the API (You'll get an *URL* and a *token*).

3. Verify that you have these (url/token) correctly set up by accessing our custom **Nivel endpoints** (see below)

4. Start with registering your vehicles **first** before you send us anything else. This is required. Sending data for unregistered vehicles will result in `400` error.

5. Proceed to send us telemetry/events/trips

# Authentication

We use *Bearer Authentication* to authenticate with our APIs

```sh
# Example

GET <path> HTTP/1.1
Host <host>
Content-Type: application/json
Accept: application/json
Authorization: Bearer <token>
..
```

# Endpoints

## Nivel

Endpoints to verify your access

```sh
# A test endpoint you can use to verify that you have the correct URL
GET <host>/ping
-> 200 - "pong"

# A test endpoint you can use to verify that you have the authentication correctly set up
GET <host>/authorized
-> 401 - "Unauthorized."
-> 200 - "Authorized!"
```

## MDS

### Supported endpoints

These endpoints are fully supported

```
POST <host>/mds/v2.0.0/agency/<city>/vehicles
PUT  <host>/mds/v2.0.0/agency/<city>/vehicles
GET  <host>/mds/v2.0.0/<city>/vehicles/<device id>
GET  <host>/mds/v2.0.0/<city>/vehicles
POST <host>/mds/v2.0.0/<city>/trips
POST <host>/mds/v2.0.0/<city>/telemetry
POST <host>/mds/v2.0.0/<city>/events
```

These are well documented [here](https://openmobilityfnd.stoplight.io/docs/mds-openapi/gqf2ey431nxzh-mobility-data-specification-open-api) and [here](https://github.com/openmobilityfoundation/mobility-data-specification/tree/2.0.0/agency)

### Unsupported endpoints

These endpoints are **not** supported. If they are important to you, contact us.

```
GET  <host>/mds/v2.0.0/vehicles/status
GET  <host>/mds/v2.0.0/vehicles/status/<device id>
POST <host>/mds/v2.0.0/stops
PUT  <host>/mds/v2.0.0/stops
GET  <host>/mds/v2.0.0/stops
GET  <host>/mds/v2.0.0/stops/<device id>
POST <host>/mds/v2.0.0/reports
```

## Car-Share

In order to get **our** usage of car sharing to work we have to introduce a new concept to MDS, "Booking". This is when a user in advance reserves a vehicle over a certain amount of time.

This is **not** the same as what MDS calls "Reservation".

So..

### Booking

```
POST <host>/mds/v2.0.0/bookings
```

Payload

```json
// Types
device_id           uuid             (required)
provider_id         uuid             (required)
booking_id          uuid             (required)
booking_time        timestamp        (required)
booked_from_time    timestamp        (required)
booked_to_time      timestamp        (required)
pickup_location     [GPS](https://github.com/openmobilityfoundation/mobility-data-specification/blob/main/data-types.md#gps-data)              (required)
drop_off_location   [GPS](https://github.com/openmobilityfoundation/mobility-data-specification/blob/main/data-types.md#gps-data)              (required)

// Example
{
  "device_id": "567a57b9-413a-41e5-9f75-7ae00b44a405",
  "provider_id": "1f129e3f-158f-4df5-af9c-a80de238e334",
  "booking_id": "ca696e72-54ec-4413-9216-90aae4f16d2c",
  "booking_time": 1699637053203,
  "booked_from_time": 1698847758442,
  "booked_to_time": 1714124842047,
  "pickup_location": {
    "lat": 19.023089138424808,
    "lng": 50.25961417907942
  },
  "drop_off_location": {
    "lat": 19.023089138424808,
    "lng": 50.25961417907942
  }
}
```

### Flow

This is the data that the provider should send the agency.

1. A user makes a booking with the provider. This is sent to the agency immediately.
2. When the providers system detects that a booking has started (`booked_from_time`) an event `reservation_start` is sent.
3. Then two things can happen:
    1. User picks up car
        * When user picks up car an event `reservation_stop` is sent.
        * When user starts driving a `trip_start` event is sent.
        * If the user takes some "breaks" the events `trip_stop` and `trip_resume` can be used to indicate this.
        * When user is done with the car an event `trip_end` is sent.
    2. User doesn't pick up the car
        * When the providers system detects that the booking has ended (`booked_to_time`) an event `trip_end` is sent.

![](./mds2-car-share-flow.jpg)
![](./mds2-car-share-diagram.jpg)
